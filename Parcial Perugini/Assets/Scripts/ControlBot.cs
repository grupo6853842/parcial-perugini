using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;

    private ControlJuego manager;

    public GameObject proyectil;
    public float bulletImpulse = 0.1f;

    void Start()
    {
        manager = GameObject.FindObjectOfType<ControlJuego>().GetComponent<ControlJuego>();
        hp = 100;
        jugador = GameObject.Find("jugador");
        InvokeRepeating("Disparar", 3f, 3f);
    }

    private void Update()
    {
        float dist = Vector3.Distance(jugador.transform.position, transform.position);
        LookAtTarget(jugador.transform.position);
        if (dist < 15f)
        {
            transform.Translate(rapidez * Vector3.back * Time.deltaTime);
        }
        if(dist > 20f)
        {
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
        

    }

    public void RecibirDaņo()
    {
        hp -= 25;
        
        if(hp <= 0)
        {
            this.Desaparecer();
            manager.Kill();
        }
    }

    private void Desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDaņo();
        }
    }

    private void Disparar()
    {
        GameObject bullet = Instantiate(proyectil, transform.position + transform.forward, transform.rotation);
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletImpulse, ForceMode.Impulse);

        Destroy(bullet.gameObject, 4);
    }

    void LookAtTarget(Vector3 target)
    {
        target.y = transform.position.y;
        transform.LookAt(target);
    }
}
