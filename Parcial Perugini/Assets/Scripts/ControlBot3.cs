using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot3 : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public GameObject enemigo4;
    public int rapidez;

    private ControlJuego manager;

    void Start()
    {
        manager = GameObject.FindObjectOfType<ControlJuego>().GetComponent<ControlJuego>();
        hp = 200;
        jugador = GameObject.Find("jugador");
    }

    private void Update()
    {
        LookAtTarget(jugador.transform.position);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    public void RecibirDaņo()
    {
        hp -= 25;

        if (hp <= 0)
        {
            this.Desaparecer();
            manager.Kill();
        }
    }

    private void Desaparecer()
    {
        Destroy(gameObject);
        Instantiate(enemigo4, transform.position, transform.rotation);
        Instantiate(enemigo4, transform.position, transform.rotation);
        Instantiate(enemigo4, transform.position, transform.rotation);
        Instantiate(enemigo4, transform.position, transform.rotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDaņo();
        }
    }

    void LookAtTarget(Vector3 target)
    {
        target.y = transform.position.y;
        transform.LookAt(target);
    }
}