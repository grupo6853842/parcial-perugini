using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot4 : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    public float magnitudSalto1;

    private Rigidbody rb;

    private ControlJuego manager;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        manager = GameObject.FindObjectOfType<ControlJuego>().GetComponent<ControlJuego>();
        hp = 25;
        jugador = GameObject.Find("jugador");
        InvokeRepeating("Saltar", 2f, 2f);
    }

    void Update()
    {
        LookAtTarget(jugador.transform.position);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    public void RecibirDaņo()
    {
        hp -= 25;

        if (hp <= 0)
        {
            this.Desaparecer();
            manager.Kill();
        }
    }

    private void Desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDaņo();
        }

        if (collision.gameObject.CompareTag("caida"))
        {
            this.Desaparecer();
            manager.Kill();
        }
    }

    private void Saltar()
    {
        rb.AddForce(0, magnitudSalto1, 0, ForceMode.Impulse);
    }

    void LookAtTarget(Vector3 target)
    {
        target.y = transform.position.y;
        transform.LookAt(target);
    }

}
