using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot5 : MonoBehaviour
{
    private int hp;
    private GameObject jugador;

    private ControlJuego manager;

    public GameObject proyectil;
    public float bulletImpulse = 0.1f;

    void Start()
    {
        manager = GameObject.FindObjectOfType<ControlJuego>().GetComponent<ControlJuego>();
        hp = 75;
        jugador = GameObject.Find("jugador");
        InvokeRepeating("Disparar", 5f, 5f);
    }

    
    void Update()
    {
        LookAtTarget(jugador.transform.position);
        float dist = Vector3.Distance(jugador.transform.position, transform.position);
    }

    public void RecibirDaņo()
    {
        hp -= 25;

        Teletransportacion();

        if (hp <= 0)
        {
            this.Desaparecer();
            manager.Kill();
        }
    }

    private void Desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDaņo();
        }
    }

    private void Disparar()
    {
        GameObject bullet = Instantiate(proyectil, transform.position + transform.forward, transform.rotation);
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletImpulse, ForceMode.Impulse);

        Destroy(bullet.gameObject, 10);
    }

    private void Teletransportacion()
    {
        float randX = Random.Range(transform.position.x - 15, transform.position.x + 15);
        float randZ = Random.Range(transform.position.z - 15, transform.position.z + 15);

        if(randX < -50 || randX > 50)
        {
            randX = Random.Range(transform.position.x - 15, transform.position.x + 15);
        }
        if(randZ < -100 || randZ > 100)
        {
            randZ = Random.Range(transform.position.z - 15, transform.position.z + 15);
        }

        transform.position = new Vector3(randX, transform.position.y, randZ);
    }

    void LookAtTarget(Vector3 target)
    {
        target.y = transform.position.y;
        transform.LookAt(target);
    }
}
