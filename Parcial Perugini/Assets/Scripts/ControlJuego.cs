using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ControlJuego : MonoBehaviour 
{ 
    public GameObject jugador; 
    public GameObject enemigo1;
    public GameObject enemigo2;
    public GameObject enemigo3;
    public GameObject enemigo5;
    public GameObject item;
    private List<GameObject> listaEnemigos;
    private List<GameObject> listaItems;

    private ControlJugador jugador1;

    public float tiempoRestante;
    public bool tiempoActivo = false;
    public int tiempoDisplay;

    private int kills = 0;

    public Text textoGanaste;
    public Text textoKills;
    public Text textoTiempo;
    public Text textoPerdiste;

    void Start()
    {
        tiempoActivo = true;
        listaEnemigos = new List<GameObject>();
        listaItems = new List<GameObject>();
        jugador1 = GameObject.FindObjectOfType<ControlJugador>().GetComponent<ControlJugador>();
        ComenzarJuego();
    }

    void Update() 
    {
        textoTiempo.text = (int)tiempoRestante + "s";

        if (tiempoActivo)
        {
            if(tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;

                if (kills == 16)
                {
                    textoGanaste.text = "Ganaste!";
                    Invoke("ComenzarJuego", 5f);
                    tiempoRestante = 0;
                    tiempoActivo = false;
                }
            }
            else
            {
                GameOver();
            }
        }

        if (Input.GetKeyDown("r"))
        {
            ComenzarJuego();
        }

        
    } 

    public void ComenzarJuego() 
    { 
        jugador.transform.position = new Vector3(0f, 1f, 0f);
        jugador1.Curar();
        foreach (GameObject item in listaEnemigos) 
        { 
            Destroy(item); 
        }
        foreach (GameObject item in listaItems)
        {
            Destroy(item);
        }
        listaEnemigos.Add(Instantiate(enemigo1, new Vector3(GenRandomPos().x, 1f, GenRandomPos().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo2, new Vector3(GenRandomPos().x, 1f, GenRandomPos().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo3, new Vector3(GenRandomPos().x, 1f, GenRandomPos().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo5, new Vector3(GenRandomPos().x, 1f, GenRandomPos().y), Quaternion.identity));

        listaEnemigos.Add(Instantiate(enemigo1, new Vector3(GenRandomPos2().x, 1f, GenRandomPos2().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo2, new Vector3(GenRandomPos2().x, 1f, GenRandomPos2().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo3, new Vector3(GenRandomPos2().x, 1f, GenRandomPos2().y), Quaternion.identity));
        listaEnemigos.Add(Instantiate(enemigo5, new Vector3(GenRandomPos2().x, 1f, GenRandomPos2().y), Quaternion.identity));
        listaItems.Add(Instantiate(item, new Vector3(GenRandomPos().x, 1f, GenRandomPos().y), Quaternion.identity));
        listaItems.Add(Instantiate(item, new Vector3(GenRandomPos2().x, 1f, GenRandomPos2().y), Quaternion.identity));
        tiempoActivo = true;
        tiempoRestante = 60;
        textoPerdiste.text = " ";
        textoGanaste.text = " ";
        kills = 0;
        textoKills.text = "Kills: " + kills.ToString();
    } 

    public void Kill()
    {
        kills++;
        textoKills.text = "Kills: " + kills.ToString();
    }

    Vector2 GenRandomPos()
    {
        Vector2 randPos;
        randPos.x = Random.Range(-50, 50);
        randPos.y = Random.Range(-100, 100);
        return (randPos);
    }
    Vector2 GenRandomPos2()
    {
        Vector2 randPos;
        randPos.x = Random.Range(70, 170);
        randPos.y = Random.Range(-100, 100);
        return (randPos);
    }

    public void GameOver()
    {
        textoPerdiste.text = "Perdiste!";
        Invoke("ComenzarJuego", 5f);
        tiempoRestante = 0;
        tiempoActivo = false;
    }
}
