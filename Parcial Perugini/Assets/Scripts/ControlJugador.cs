using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;

    public GameObject proyectil;
    public int velocidadPro;
    public Transform puntoDisp;

    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;

    private Rigidbody rb;
    private ControlJuego manager;

    int salto = 1;

    public int hp;

    public Text textoHP;

    void Start()
    {
        hp = 100;
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        manager = GameObject.FindObjectOfType<ControlJuego>().GetComponent<ControlJuego>();
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, puntoDisp.position, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * velocidadPro, ForceMode.Impulse);

            Destroy(pro, 3);
        }

        if (Input.GetKeyDown(KeyCode.Space) && salto!=0)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            salto--;
        }
        if (EstaEnPiso())
        {
            salto = 1;
        }

        textoHP.text = "HP: " + hp.ToString();
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    public void RecibirDa�o(int da�o)
    {
        hp -= da�o;

        if (hp <= 0)
        {
            hp = 100;
            manager.GameOver();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            RecibirDa�o(20);
        }
        if (collision.gameObject.CompareTag("bump"))
        {
            RecibirDa�o(15);
        }
        if (collision.gameObject.CompareTag("caida"))
        {
            manager.GameOver();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("item"))
        {
            other.gameObject.SetActive(false);
            rapidezDesplazamiento += 10.0f;
            transform.localScale = transform.localScale * 2;
            Invoke("PowerUp", 5f);
        }
    }

    private void PowerUp()
    {
        rapidezDesplazamiento -= 10.0f;
        transform.localScale = transform.localScale / 2;
    }

    public void Curar()
    {
        hp = 100;
    }
}
