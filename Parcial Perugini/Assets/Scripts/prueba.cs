using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prueba : MonoBehaviour
{
    private Rigidbody rb;
    public float magnitudSalto2;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        InvokeRepeating("Saltar", 1f, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Saltar()
    {
        Debug.Log("salto");
        rb.AddForce(0, magnitudSalto2, 0, ForceMode.Impulse);
    }
}
